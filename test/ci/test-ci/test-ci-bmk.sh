#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  # Configure WL copy
  # Execute WL copy
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  if [ "$NEVENTS_THREAD" == "1" ]; then
    echo "Hallo World! (FASTER DUMMY TEST)" >out_$1.log 2>&1
  elif [ "$NEVENTS_THREAD" == "2" ]; then
    source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh >out_$1.log 2>&1 \
      && echo "LHCb setup (DEFAULT DUMMY TEST)" >>out_$1.log 2>&1
  else
    source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh >out_$1.log 2>&1 \
      && lb-run --use-sp -c x86_64-slc6-gcc48-opt --use="AppConfig v3r335" --use="DecFiles v30r11" --use="ProdConf" Gauss/v49r9 echo "LHCb Gauss setup (SLOWER DUMMY TEST)" >>out_$1.log 2>&1
  fi
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NEVENTS_THREAD =1 : dummy HalloWorld test (faster)"
  echo "               =2 : dummy LHCb setup test (default)"
  echo "               >2 : dummy LHCb Gauss setup test (slower)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NCOPIES=$(nproc)
###NEVENTS_THREAD=1 # DUMMY HalloWorld TEST (FASTER)
NEVENTS_THREAD=2 # DUMMY LHCb setup TEST (DEFAULT)
###NEVENTS_THREAD=3 # DUMMY LHCb Gauss setup TEST (SLOWER)

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
