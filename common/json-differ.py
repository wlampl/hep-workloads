# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

import sys
import json
from dictdiffer import diff

json_list = []
for ajson in sys.argv[1:3]:
    print("Reading file %s" % ajson)
    json_list.append( json.load(open(ajson)) )
        

result = list(diff(json_list[0], json_list[1]))

for entry in result:
    if len(entry[2]) == 1:
        print('\n\t %s :\n\t\t %s\t%s' % entry)
    else:
        print('\n\t %s :\n\t\t %s\n\t\t\t%s\n\t\t\t%s' % (entry[0],entry[1],entry[2][0],entry[2][1]))
        
exit(len(result))



