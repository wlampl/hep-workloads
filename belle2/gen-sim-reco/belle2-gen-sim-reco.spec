HEPWL_BMKEXE=belle2-gen-sim-reco-bmk.sh
HEPWL_BMKOPTS="-e 10 -c 1"
HEPWL_BMKDIR=belle2-gen-sim-reco
HEPWL_BMKDESCRIPTION="belle2-gen-sim-reco-bmk"
HEPWL_DOCKERIMAGENAME=belle2-gen-sim-reco-bmk
HEPWL_DOCKERIMAGETAG=v2.0
HEPWL_CVMFSREPOS=belle.cern.ch
