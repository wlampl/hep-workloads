#!/bin/bash

set -x 
set -e  
BODY="{
    \"branch\": \"${TMP_BRANCH}\",
    \"ref\": \"${CI_COMMIT_SHA}\"
}";

curl -X POST  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/branches" \
    --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}"

BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${TMP_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"title\": \"merge ${CI_COMMIT_BRANCH} commit ${CI_COMMIT_SHORT_SHA} from ${CI_PIPELINE_ID}\",
    \"remove_source_branch\": true,
    \"allow_collaboration\": false
}";

curl -X POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests" \
          --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
          --header "Content-Type: application/json" \
          --data "${BODY}" 



