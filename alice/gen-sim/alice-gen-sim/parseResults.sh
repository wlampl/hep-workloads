#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

function generateSummary(){
  echo -e "{
    \"run_info\": {\"copies\":$NCOPIES , \"threads_per_copy\":$NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD },
     \"report\": {$resJSON , \"log\": \"${s_msg}\"}, 
     \"app\": `jq --arg flavor $flavor '. + { "containment" : $flavor }' $BMKDIR/version.json` }" > ${APP}_summary.json
  cat ${APP}_summary.json
}

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseresults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  export resJSON='"wl-scores":{}'
  export s_msg="ok"
  if [ "$1" -ne 0 ]; then
    echo "Previous steps failed: skip parsing, go to generateSummary"
    s_msg="ERROR"
    generateSummary # this has no return code
    return 1
  fi  
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] parsing results from" proc_*/time_useronly.log
  # Parsing  Event Throughput: xxxx ev/s
  resJSON=` cat proc_*/time_useronly.log | awk -v nevents=$NEVENTS_THREAD '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=( int(nevents*1.) / int($1*1.) ); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
      if (n % 2) {
        median=a[(n + 1) / 2];
      } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
      }; 
printf "\"wl-scores\": {\"gen-sim\": %.4f} , \"wl-stats\": {\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}",
sum, sum/count, median, amin, amax, count
    }' || (echo "\"wl-scores\":{}"; exit 1)`
  STATUS=$?
  echo "[parseResults] checking that all job are completed correctly from" proc_*/out_*.log
  ALL_JOBS_SUCCEED=`grep -c -h -m1 "finished with the expected exit code" proc_*/out_*.log | awk 'BEGIN{sum=0;count=0}{sum+=$1;count+=1}END{print sum==count}'`
  if [ "$ALL_JOBS_SUCCEED" != 1 ]; then
    STATUS=1
    s_msg="[ERROR] There is at least one job not finished properly"
    resJSON='"wl-scores":{}'
    grep -c "finished with the expected exit code" proc_*/out_*.log
  fi
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate summary"
  generateSummary # this has no return code
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $STATUS
}
