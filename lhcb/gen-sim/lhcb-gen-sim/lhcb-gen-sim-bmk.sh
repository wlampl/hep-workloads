#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Configure WL copy
  cat $BMKDIR/prodConf_Gauss_00071400_00000089_1.py | sed -e "s/NOfEvents=3/NOfEvents=${NEVENTS_THREAD}/" > prodConf_Gauss_00071400_00000089_1.py
  # Execute WL copy
  unset -f doOne # workaround for LBCORE-1787 within BMK-166
  strace=""
  ###strace="strace -tt -f -o out_$1_strace0.txt" # optionally produce a strace output
  # Use process substitution to tee stderr both to log and stderr (https://stackoverflow.com/a/692407)
  # Unlike a pipe, this retains the command exit code (no need for 'PIPESTATUS' or 'set -o pipefail')
  ###set -x # assume x was not already in $-
  source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh > out_$1.log 2> >(tee -a out_$1.log >&2) && \
    ${strace} lb-run --use-sp -c x86_64-slc6-gcc48-opt --use="AppConfig v3r335" --use="DecFiles v30r11" --use="ProdConf" Gauss/v49r9 gaudirun.py -T '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py' '$APPCONFIGOPTS/Gauss/DataType-2016.py' '$APPCONFIGOPTS/Gauss/RICHRandomHits.py' '$DECFILESROOT/options/27163076.py' '$LBPYTHIA8ROOT/options/Pythia8.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' "./prodConf_Gauss_00071400_00000089_1.py" >> out_$1.log 2> >(tee -a out_$1.log >&2)
  ###set +x # assume x was not already in $-
  status=${?}
  ###if [ "$strace" != ""  ]; then cat out_$1_strace0.txt | grep '"/cvmfs' | awk '{p=$0; while(1){ i1=index(p,"\"/cvmfs"); if (i1<=0) break; p=substr(p,i1+1); i2=index(p,"\""); if (substr(p,i2+1,3)!="...") {print substr(p,0,i2-1)}; p=substr(p,i2+1)} }' | sort -u > out_$1_strace1.txt; fi
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=5

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
