# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

if [ "$BASH_SOURCE" = "$0" ]; then echo "ERROR! This script ($0) was not sourced"; exit 1; fi
if [ "$BASH_SOURCE" = "" ]; then echo "ERROR! This script was not sourced from bash"; return 1; fi
echo "[$(basename ${BASH_SOURCE})] Loading functions from $(basename ${BASH_SOURCE})"
###if [ "$(type -t generate_Dockerfile)" == "function" ]; then echo "WARNING! Function generate_Dockerfile already loaded"; fi


# Generate workload-specific Dockerfile from the common Dockerfile.template,
# using the environment variables HEPWL_xxx defined in the workload-specific spec file
# and appending the workload-specific Dockerfile.append if it exists.
# Input arguments: -s <full path to HEP workload spec file> -d <full path to Dockerfile template>.
# No environment variables from the calling script are used in this function (BMK-117,BMK-118).

function generate_Dockerfile(){

  local hepwlSpecFile=""
  local dockerCommonHeader=""
  local dockerCommonTemplate=""

  local OPTIND # used by bash builtin command 'getopts'
  local OPTARG # used by bash builtin command 'getopts'

  while getopts "hs:H:T:" o; do
    case ${o} in
      s)
        [ "$OPTARG" != "" ] && hepwlSpecFile="$OPTARG"
        ;;
      H)
        [ "$OPTARG" != "" ] && dockerCommonHeader="$OPTARG"
        ;;
      T)
        [ "$OPTARG" != "" ] && dockerCommonTemplate="$OPTARG"
        ;;
      *)
        echo "Usage: ${FUNCNAME[0]} -s <hepwlSpecFile> -H <dockerCommonHeader> -T <dockerCommonTemplate>"
        return 1
        ;;
    esac
  done

  echo "[${FUNCNAME[0]}] Input hepwlSpecFile: $hepwlSpecFile"
  echo "[${FUNCNAME[0]}] Input dockerCommonHeader: $dockerCommonHeader"
  echo "[${FUNCNAME[0]}] Input dockerCommonTemplate: $dockerCommonTemplate"

  if [ "$hepwlSpecFile" == "" ]; then
    echo "ERROR! Not specified: hepwlSpecFile"
    ${FUNCNAME[0]} -h
    return 1
  fi

  if [ "$dockerCommonHeader" == "" ]; then
    echo "ERROR! Not specified: dockerCommonHeader"
    ${FUNCNAME[0]} -h
    return 1
  fi

  if [ "$dockerCommonTemplate" == "" ]; then
    echo "ERROR! Not specified: dockerCommonTemplate"
    ${FUNCNAME[0]} -h
    return 1
  fi

  if [ ! -f $hepwlSpecFile ]; then
    echo "ERROR! File not found: $hepwlSpecFile"
    return 1
  fi

  if [ ! -f $dockerCommonHeader ]; then
    echo "ERROR! File not found: $dockerCommonHeader"
    return 1
  fi

  if [ ! -f $dockerCommonTemplate ]; then
    echo "ERROR! File not found: $dockerCommonTemplate"
    return 1
  fi

  if ! load_and_validate_specfile "$hepwlSpecFile"; then
    echo "ERROR! invalid SPEC file"
    return 1
  fi  

  dockerFile="$(dirname $hepwlSpecFile)/Dockerfile"

  cat $dockerCommonHeader | sed -e "s@linuxsupport/slc6-base:latest@linuxsupport/${HEPWL_BMKOS}-base:latest@g" > $dockerFile

  hepwlDockerAppend="$(dirname hepwlSpecFile)/Dockerfile.append"
  if [ -f ${hepwlDockerAppend} ]; then 
    cat ${hepwlDockerAppend} | sed -e "s@__insert_bmkdir__@./$HEPWL_BMKDIR@g" \
      -e "s@__insert_bmkdescription__@$HEPWL_BMKDESCRIPTION@g" \
      -e "s@__insert_bmkexe__@./$HEPWL_BMKDIR/$HEPWL_BMKEXE@g" >> $dockerFile
  fi

  cat $dockerCommonTemplate | sed -e "s@__insert_bmkdir__@./$HEPWL_BMKDIR@g" \
    -e "s@__insert_bmkdescription__@$HEPWL_BMKDESCRIPTION@g" \
    -e "s@__insert_bmkexe__@./$HEPWL_BMKDIR/$HEPWL_BMKEXE@g" \
    -e "s@__insert_bmktag__@$HEPWL_DOCKERIMAGETAG@g" >> $dockerFile

  echo "[${FUNCNAME[0]}] Successfully generated: $dockerFile"

}
